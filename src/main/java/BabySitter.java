
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;


public class Babysitter {
	
	private static final LocalTime MIN_START_TIME= LocalTime.parse("17:00");
	private static final LocalTime MAX_END_TIME= LocalTime.parse("04:00");
	private static final LocalTime BEDTIME= LocalTime.parse("21:00");
	private static final LocalTime MIDNIGHT = LocalTime.MIDNIGHT;
	
	private static final int RATE_FROM_START_UNTIL_BEDTIME= 12;
	private static final int RATE_FROM_BEDTIME_UNTIL_MIDNIGHT= 8;
	private static final int RATE_FROM_MIDNIGHT_UNTIL_MAX_END_TIME= 16;
	
	
	
	
	private String startTime;
	private String endTime;
	private int hoursWorked;
	
	public Babysitter() {
		
	}
	
	public Babysitter(String startTime, String endTime) {
		this.startTime= startTime;
		this.endTime= endTime;
	}
	
	public LocalTime getStartTime() {
		LocalTime localTimeStartTime=LocalTime.parse(startTime);

		return localTimeStartTime;
	}
	
	public LocalTime getEndTime() {
		LocalTime localTimeEndTime=LocalTime.parse(endTime);
		return localTimeEndTime;
	}
	
	public void SetHoursWorked(int hoursWorked) {
		this.hoursWorked= hoursWorked;
	}
	
	public int getHoursWorked() {
		
		return hoursWorked;
	}
	
	
	
	public boolean scheduleIsValid() {
		if (LocalTime.parse(startTime).isBefore(MIN_START_TIME)) {
			return false;
		}
		else if (LocalTime.parse(endTime).isAfter(MAX_END_TIME)) {
			return false;
		}
		else return true;
	}
	
	
	
	public int calculateHoursWorked() {
		
		
		Map <LocalTime, Integer> timeMap = new HashMap<LocalTime, Integer>();
		timeMap.put(LocalTime.parse("17:00"),17);
		timeMap.put(LocalTime.parse("18:00"),18);
		timeMap.put(LocalTime.parse("19:00"),19);
		timeMap.put(LocalTime.parse("20:00"),20);
		timeMap.put(LocalTime.parse("21:00"),21);
		timeMap.put(LocalTime.parse("22:00"),22);
		timeMap.put(LocalTime.parse("23:00"),23);
		timeMap.put(LocalTime.parse("00:00"),24);
		timeMap.put(LocalTime.parse("01:00"),25);
		timeMap.put(LocalTime.parse("02:00"),26);
		timeMap.put(LocalTime.parse("03:00"),27);
		timeMap.put(LocalTime.parse("04:00"),28);
		
		
		
		int intStartTime=timeMap.get(getStartTime());
		int intEndTime=timeMap.get(getEndTime());
		int numberHoursWorked=intEndTime-intStartTime;
		
		SetHoursWorked(numberHoursWorked);
		
		return numberHoursWorked;
		
	}
	
	public int calculatePayment() {
		
		Map <LocalTime, Integer> timeMap = new HashMap<LocalTime, Integer>();
		timeMap.put(LocalTime.parse("17:00"),17);
		timeMap.put(LocalTime.parse("18:00"),18);
		timeMap.put(LocalTime.parse("19:00"),19);
		timeMap.put(LocalTime.parse("20:00"),20);
		timeMap.put(LocalTime.parse("21:00"),21);
		timeMap.put(LocalTime.parse("22:00"),22);
		timeMap.put(LocalTime.parse("23:00"),23);
		timeMap.put(LocalTime.parse("00:00"),24);
		timeMap.put(LocalTime.parse("01:00"),25);
		timeMap.put(LocalTime.parse("02:00"),26);
		timeMap.put(LocalTime.parse("03:00"),27);
		timeMap.put(LocalTime.parse("04:00"),28);
		
		
		
		int intStartTime=timeMap.get(getStartTime());
		int intEndTime=timeMap.get(getEndTime());
		
		int bedTime= timeMap.get(BEDTIME);
		int midnight= timeMap.get(MIDNIGHT);
		int minStartTime= timeMap.get(MIN_START_TIME);
		int maxEndTime= timeMap.get(MAX_END_TIME);
		
		 
		int payment=0;
		int hoursWorked=calculateHoursWorked();
		
		
		if ((intStartTime <= bedTime) && ((intEndTime >= bedTime) && (intEndTime <= midnight))) {
	 		int startTimeToBedtimePayment=(bedTime-intStartTime)*RATE_FROM_START_UNTIL_BEDTIME;
	 		int bedtimeToEndTime=(intEndTime- bedTime)*RATE_FROM_BEDTIME_UNTIL_MIDNIGHT;
	 		
	 		payment=startTimeToBedtimePayment + bedtimeToEndTime;
	 		
	 	} else if ((intStartTime <= bedTime) && ((intEndTime >= midnight) && (intEndTime <= maxEndTime))) {
	 		int startTimeToBedtimePayment=(bedTime-intStartTime)*RATE_FROM_START_UNTIL_BEDTIME;
	 		int bedtimeToMidnight=(midnight- bedTime)*RATE_FROM_BEDTIME_UNTIL_MIDNIGHT;
	 		int MidnightToMaxEndTime=(intEndTime-midnight)*RATE_FROM_MIDNIGHT_UNTIL_MAX_END_TIME;
	 		
	 		payment=startTimeToBedtimePayment + bedtimeToMidnight + MidnightToMaxEndTime; 
	 	}	
		else if ((intStartTime >= midnight) && (intEndTime <= maxEndTime)) {
			payment=hoursWorked*RATE_FROM_MIDNIGHT_UNTIL_MAX_END_TIME;
			
		} else if ((intStartTime >= bedTime) && (intEndTime <= midnight)){
						payment=hoursWorked*RATE_FROM_BEDTIME_UNTIL_MIDNIGHT;
						
		} else if ((intStartTime >= minStartTime) && (intEndTime <= bedTime)) {
			 payment=hoursWorked*RATE_FROM_START_UNTIL_BEDTIME;
			 
		}

		return payment;
	}
}
