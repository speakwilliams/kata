
import java.time.LocalTime;

import org.junit.Assert;

import org.junit.Test;



public class BabysitterTest {
	

	
	@Test 
	public void babysitter_returns_start_time_and_end_time_test() {
		Babysitter babysitter= new Babysitter("10:00", "14:00");
		
		LocalTime startTime=babysitter.getStartTime();
		LocalTime endTime=babysitter.getEndTime();
		
		Assert.assertEquals( LocalTime.of(10, 00), startTime);
		Assert.assertEquals( LocalTime.of(14, 00), endTime);

	}
	
	@Test
	public void babysiiter_cant_work_before_10pm_through_4pm_test() {
		Babysitter babysitter= new Babysitter("10:00", "04:00");
		
		babysitter.scheduleIsValid();
		
		Assert.assertFalse("Schedule should not be valid if before 5:00pm", babysitter.scheduleIsValid());
		Assert.assertFalse("Schedule should not be valid if after 4:00am", babysitter.scheduleIsValid());
	}
	
	@Test
	public void calculate_amount_of_hours_worked_six_pm_through_three_am_test() {
		Babysitter babysitter= new Babysitter("18:00", "03:00");
		
		int hours=babysitter.calculateHoursWorked();
		
		
		Assert.assertEquals(9, hours);
		
	}
	
	@Test
	public void calculate_amount_of_hours_worked_midnight_through_four_pm_test() {
		Babysitter babysitter= new Babysitter("00:00", "04:00");
		
		int hours=babysitter.calculateHoursWorked();
			
		Assert.assertEquals(4, hours);
		
	}
	
	@Test
	public void calculate_amount_of_hours_worked_five_pm_through_four_pm_test() {
		Babysitter babysitter= new Babysitter("17:00", "04:00");
		
		int hours=babysitter.calculateHoursWorked();
		
		Assert.assertEquals(11, hours);
		
	}
	
	@Test
	public void calculate_payment_five_pm_through_nine_pm_test() {
		Babysitter babysitter= new Babysitter("17:00", "21:00");
		
		int payment=babysitter.calculatePayment();
		
		Assert.assertEquals(48, payment);
	}

	
	@Test
	public void calculate_payment_five_pm_through_bedtime_test() {
		Babysitter babysitter= new Babysitter("17:00", "19:00");
		
		int payment=babysitter.calculatePayment();
		
		Assert.assertEquals(24, payment);
	}
	
	@Test 
	public void calculate_payment_bedtime_through_midnight_test() {
		Babysitter babysitter = new Babysitter("21:00", "00:00");
		
		int payment= babysitter.calculatePayment();
		
		Assert.assertEquals(24, payment);
		
	}
	
	@Test 
	public void calculate_payment_ten_pm_through_midnight_test() {
		Babysitter babysitter = new Babysitter("22:00", "00:00");
		
		int payment= babysitter.calculatePayment();
		
		Assert.assertEquals(16, payment);
		
	}
	
	@Test 
	public void calculate_payment_midnight_through_four_am_test() {
		Babysitter babysitter = new Babysitter("00:00", "04:00");
		
		int payment = babysitter.calculatePayment();
		
		Assert.assertEquals(64, payment);
		
	}
	@Test 
	public void calculate_payment_two_am_through_four_am_test() {
		Babysitter babysitter = new Babysitter("02:00", "04:00");
		
		int payment = babysitter.calculatePayment();
		
		Assert.assertEquals(32, payment);
		
	}
	@Test
	public void calculate_payment_five_pm_through_midnight_test() {
		Babysitter babysitter= new Babysitter("17:00", "00:00");
		
		int payment = babysitter.calculatePayment();
		
		Assert.assertEquals(72, payment);
	}
	
	@Test
	public void calculate_payment_seven_pm_throuhg_ten_pm_test() {
		Babysitter babysitter= new Babysitter("19:00", "22:00");
		
		int payment = babysitter.calculatePayment();
		
		Assert.assertEquals(32, payment);
	}
	
	@Test
	public void calculate_payment_five_pm_through_four_am() {
		Babysitter babysitter = new Babysitter("17:00", "04:00");
		
		int payment = babysitter.calculatePayment();
		
		Assert.assertEquals(136, payment);
	}
	
	@Test
	public void calculate_payment_seven_pm_through_one_am() {
		Babysitter babysitter = new Babysitter("19:00", "01:00");
		
		int payment = babysitter.calculatePayment();
		
		Assert.assertEquals(64, payment);	
	}
	@Test
	public void calculate_payment_bedtime_through_4_am() {
		Babysitter babysitter = new Babysitter("21:00", "04:00");
		
		int payment= babysitter.calculatePayment();
		
		Assert.assertEquals(88, payment);
			
	}

}
